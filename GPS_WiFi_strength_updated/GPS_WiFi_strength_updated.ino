#include "BluetoothSerial.h"
#include "WiFi.h"
#include "EEPROM.h"
#include <HardwareSerial.h>
#include <TinyGPS++.h>

#define EEPROM_SIZE 512

#define GPS_LED 4
#define WIFI_LED 15

String ssid_saved_ = "";
String password_saved_ = "";
String ssid_header_ = "s";
String password_header_ = "p";
int ssid_address_ = 0;
int password_address_ = 200;
const char* ssid_;
const char* password_;
String starter_ = "Start";
String resetter_ = "Reset";
bool started_ = false;

bool publish_now_ = false;

int loop_delay_address_ = 400;
unsigned long loop_delay_ = 100;

BluetoothSerial SerialBT;

TinyGPSPlus gps;

#define SerialGPS Serial2

void setup() {
  Serial.begin(115200);
  SerialBT.begin("GPS + WiFi Meter"); //Bluetooth device name
  Serial2.begin (9600);
  Serial.println("The device started, now you can pair it with bluetooth!");
  pinMode(GPS_LED, OUTPUT);
  pinMode(WIFI_LED, OUTPUT);
  digitalWrite(GPS_LED, LOW);
  digitalWrite(WIFI_LED, LOW);
  delay(1000);
  EEPROM.begin(EEPROM_SIZE);
  delay(1000);

  //EEPROM.get(loop_delay_address_, loop_delay_);
  //(loop_delay_ < 1) ? loop_delay_ = 1000 : loop_delay_ = loop_delay_;
  String incoming;
  String header;
  uint8_t pos;

  while (!started_)
  {
    SerialBT.println("Send 'Start'");
    delay(1000);
    incoming = SerialBT.readString();
    if (incoming == starter_)
    {
      Serial.println("Starting");
      SerialBT.println("Starting");
      started_ = true;
      break;
    }
    else if (incoming == resetter_)
    {
      Serial.println("Resetting");
      SerialBT.println("Resetting");
      delay(500);
      ESP.restart();
    }

    pos = incoming.indexOf("=");
    header = incoming.substring(0, pos);
    if ( header == ssid_header_)
    {
      String ssid_recieved = incoming.substring(pos + 1, incoming.length());
      uint8_t ssid_length = ssid_recieved.length() + 1;
      char ssid_char[ssid_length];
      ssid_recieved.toCharArray(ssid_char, ssid_length);

      EEPROM.put(ssid_address_, ssid_length);
      EEPROM.commit();
      delay(1000);

      for (uint8_t i = 0; i < ssid_length; i++ )
      {
        EEPROM.put(i + ssid_address_ + 4, ssid_char[i]);
      }
      EEPROM.commit();
      delay(1000);

      Serial.println("Resetting");
      SerialBT.println("Resetting");
      delay(500);
      ESP.restart();
    }
    else if ( header == password_header_)
    {
      String password_recieved = incoming.substring(pos + 1, incoming.length());
      uint8_t password_length = password_recieved.length() + 1;
      char password_char[password_length];
      password_recieved.toCharArray(password_char, password_length);

      EEPROM.put(password_address_, password_length);
      EEPROM.commit();
      delay(1000);

      for (uint8_t i = 0; i < password_length; i++ )
      {
        EEPROM.put(i + password_address_ + 4, password_char[i]);
      }
      EEPROM.commit();
      delay(1000);

      Serial.println("Resetting");
      SerialBT.println("Resetting");
      delay(500);
      ESP.restart();
    }
  }

  uint8_t ssid_get_length;
  EEPROM.get(ssid_address_, ssid_get_length);
  ssid_get_length -= 1;
  char ssid_get_char[ssid_get_length];
  for (uint8_t i = 0; i < ssid_get_length; i++ )
  {
    EEPROM.get(i + ssid_address_ + 4, ssid_get_char[i]);
    ssid_saved_.concat(ssid_get_char[i]);
  }

  uint8_t password_get_length;
  EEPROM.get(password_address_, password_get_length);
  password_get_length -= 1;
  char password_get_char[password_get_length];
  for (uint8_t i = 0; i < password_get_length; i++ )
  {
    EEPROM.get(i + password_address_ + 4, password_get_char[i]);
    password_saved_.concat(password_get_char[i]);
  }

  SerialBT.print("Connecting to '");
  SerialBT.print(ssid_saved_);
  SerialBT.print("'  With password   ");
  SerialBT.println(password_saved_);

  Serial.print("Connecting to '");
  Serial.print(ssid_saved_);
  Serial.print("'  With password   ");
  Serial.println(password_saved_);

  const char* ssid_temp = ssid_saved_.c_str();
  ssid_ = ssid_temp;

  const char* password_temp = password_saved_.c_str();
  password_ = password_temp;

  //WiFi.begin(ssid_, password_);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(1000);

  SerialBT.print(",");
  SerialBT.print("Longitude"); SerialBT.print(",");
  SerialBT.print("Latitude"); SerialBT.print(",");
  SerialBT.print("RSSI"); SerialBT.print(",");
  SerialBT.print("BSSID"); SerialBT.print(",");
  SerialBT.print("SSID"); SerialBT.print(",");
  SerialBT.print("Channel"); SerialBT.print(",");
  SerialBT.print("Altitude"); SerialBT.print(",");
  SerialBT.print("Speed"); SerialBT.print(",");
  SerialBT.print("No of satellites"); SerialBT.print(",");
  SerialBT.print("hdop");//SerialBT.print(",");
  SerialBT.print("\n");
  delay(1000);
  SerialBT.flush();
}


void loop() {
  /*if ((WiFi.status() == WL_CONNECTED) && (WiFi.RSSI() < -75))
    {
    WiFi.disconnect();
    delay(2000);
    digitalWrite(WIFI_LED, LOW);
    }*/

  /*if (WiFi.status() != WL_CONNECTED) //wifi not connected
    {
    publish_now_ = false;
    digitalWrite(WIFI_LED, LOW);
    Serial.print(".");
    WiFi.begin(ssid_, password_);
    delay(1000);
    }*/


  if (SerialBT.available())
  {
    String incoming = SerialBT.readString();
    if (incoming == resetter_)
    {
      Serial.println("Resetting");
      SerialBT.println("Resetting");
      delay(500);
      ESP.restart();
    }
    else
    {
      float frequency = incoming.toFloat();
      loop_delay_ = 1000 / frequency;
      //EEPROM.put(loop_delay_address_, loop_delay_);
      //EEPROM.commit();
    }
    /*
      uint8_t pos = incoming.indexOf("=");
      String header = incoming.substring(0, pos);
      if ( header == "f")
      {
      String frequency_recieved = incoming.substring(pos + 1, incoming.length());
      float frequency = frequency_recieved.toFloat();
      loop_delay_ = 1000 / frequency;
      EEPROM.put(loop_delay_address_,loop_delay_);
      EEPROM.commit();
      //Serial.println(loop_delay_);
      }*/

  }
  Serial.println("here");
  if ((SerialGPS.available() > 0))
  {
    Serial.println("here1");
    if (gps.encode(SerialGPS.read()))
    {
      Serial.println("here2");
      int n = WiFi.scanNetworks();
      int rssi = -1000;
      int index = 0;
      if (n == 0)
      {
        digitalWrite(WIFI_LED, LOW);
        publish_now_ = false;
      }
      else {
        publish_now_ = true;
        digitalWrite(WIFI_LED, HIGH);
        for (uint8_t i = 0; i < n; i++)
        {
          if (WiFi.SSID(i) == ssid_saved_)
          {
            if ( rssi < WiFi.RSSI(i))
            {
              index = i;
              rssi = WiFi.RSSI(i);
            }
          }
        }
        //Serial.println(rssi);
      }

      if (rssi == -1000)
      {
        digitalWrite(WIFI_LED, LOW);
        publish_now_ = false;
      }
      if ((gps.location.isValid()) && (gps.satellites.value() > 0))
      {
        digitalWrite(GPS_LED, HIGH);
        if (publish_now_)
        {
          SerialBT.print(",");

          //SerialBT.print("Longitude = ");
          SerialBT.print(gps.location.lng(), 10);
          SerialBT.print(",");

          //SerialBT.print("Latitude = ");
          SerialBT.print(gps.location.lat(), 10);
          SerialBT.print(",");

          //SerialBT.print("RSSI : ");
          SerialBT.print(rssi);
          SerialBT.print(",");

          //SerialBT.print("BSSID : ");
          SerialBT.print(WiFi.BSSIDstr(index));
          SerialBT.print(",");

          //SerialBT.print("SSID : ");
          SerialBT.print(ssid_saved_);
          SerialBT.print(",");

          //SerialBT.print("Channel : ");
          SerialBT.print(WiFi.channel(index));
          SerialBT.print(",");

          //SerialBT.print("Altitude = ");
          SerialBT.print(gps.altitude.meters(), 3);
          SerialBT.print(",");

          //SerialBT.print("Speed = ");
          SerialBT.print(gps.speed.kmph());
          SerialBT.print(",");

          //SerialBT.print("No of satellites = ");
          SerialBT.print(gps.satellites.value());
          SerialBT.print(",");

          //SerialBT.print("hdop= ");
          SerialBT.print(gps.hdop.hdop(), 3);
          //SerialBT.print(",");

          SerialBT.print("\n");

          delay(loop_delay_);
        }
      }
      else
      {
        digitalWrite(GPS_LED, LOW);
      }
    }
  }
}
