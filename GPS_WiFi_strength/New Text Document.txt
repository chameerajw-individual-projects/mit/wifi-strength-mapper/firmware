while (SerialGPS.available() > 0) {
    if (gps.encode(SerialGPS.read()))
    {
      if (gps.location.isValid())
      {
        latitude = gps.location.lat();
        lat_str = String(latitude , 6);
        longitude = gps.location.lng();
        lng_str = String(longitude , 6);

        Serial.print("Latitude = ");
        Serial.println(lat_str);

        Serial.print("Longitude = ");
        Serial.println(lng_str);

        Serial.print("sats = ");
        Serial.println(gps.satellites.value());

        Serial.print("speed = ");
        Serial.println(gps.speed.kmph());

        Serial.print("hdop= ");
        Serial.println(gps.hdop.hdop());

        Serial.print("course= ");
        Serial.println(gps.course.deg());
      }
    }
  }